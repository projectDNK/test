<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task1</title>
</head>
<body>
<form action="index.php" method="post">
    <label for="textarea">Input text</label>
    <textarea id="textarea" rows="2" cols="20" name="textarea"></textarea>
    <input type="submit">
</form>
</body>
</html>


<?php

function getData(){
    $data = $_POST['textarea'];
    $arrayOfStrings = str_replace("\n", ' ', $data);
    $arrayOfStrings = explode(' ', $arrayOfStrings);
    return $arrayOfStrings;
}
function variantOne($arrayOfStrings){
    $replacement = [];
    foreach ($arrayOfStrings as $value){
        if (iconv_strlen($value) > 5) {
            $replacement[] = mb_substr($value, 0, 5) . '*';
        }
        else {
            $replacement[] = $value;
        }
    }
    return $replacement;
}
function variantTwo($arrayOfStrings){
    $replacement = [];
    $pattern = '/([а-яА-ЯёЁіІa-zA-Z0-9_]{5})([^ ]+)/ui'; //   /(\w{5})([^ ]+)/
    foreach ($arrayOfStrings as $value) {
        $replacement [] = preg_replace($pattern, '$1*', $value);
    }
return $replacement;
}

function output($numOfVariant, $outputArray){
    if ($numOfVariant == 1){
        echo 'First variant: ';
    }
    elseif ($numOfVariant == 2){
         echo '<br>' . 'Second variant: ';
    }

    foreach ($outputArray as $item){
        echo $item . ' ';
    }
}
if($_POST['textarea']){
    $data = getData();
    $tmp = variantOne($data);
    output(1,$tmp);
    unset($tmp);
    $tmp = variantTwo($data);
    output(2, $tmp);
}
?>