<?php

function checking($string){
    $pattern = '/http?(s)?.:\/\//';
    if(preg_match($pattern, $string, $matches, PREG_OFFSET_CAPTURE)) {//);
        return $matches[0][0];
    }
    else
        return 'No matches found';
}

function genData(){
    $data = array(
        '0' => 'https://www.google.ua/',
        '1' => 'https://www.instagram.com/',
        '2' => 'http://vk.com/',
        '3' => 'https://www.youtube.com/',
        '4' => 'http://www.facebook.com/',
        '5' => 'www.google.com.ua',
        '6' => 'www.yandex.ua',
        '7' => 'www.ukr.net'
    );
    return $data;
}

function result()
{
    $string = genData();
    for ($i = 0; $i < count($string); $i++) {
        echo '<pre>';
        echo $string[$i] . ' | ' . checking($string[$i]);
        echo '</pre>';
    }
}
result();
