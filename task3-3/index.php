<?php
include ('../task3-1/functions.php');
header('Content-Type: text/html; charset=windows-1251');
$fd = fopen('../task3-1/dates.csv', 'r');

$content = readFromCSV($fd);

array_pop($content);
output($content, 'Init');

$usort = customSortOne($content);
output($usort, 'usort()');


$uasort = customSortTwo($content);
output($uasort, 'uasort()');

$array_multisort = customSortThree($content);
output($array_multisort, 'array_multisort()');


function output($dates, $sortname){
    echo $sortname.':<br>';
    foreach ($dates as $item){
        echo  $item['Date'].' '. $item['Number']/*.' '.$item['Name']*/. '<br>';
    }
}

function getDates($content){
    $dates = [];
    foreach ($content as $item){
        $dates [] = strtotime($item['Date']);
    }
    return $dates;
}

function compare($a, $b){
    if($a['Number'] == $b['Number']) return 0;
        return ($a['Number'] < $b['Number']) ? 1 : -1;
}

function customSortOne(&$dates){
    usort($dates, "compare");
    return $dates;
}

function customSortTwo(&$dates){
    uasort($dates, "compare");
    return $dates;
}

function customSortThree(&$dates){
    foreach ($dates as $key => $row)
    {
        $sorted[$key]  = $row['Number'];
    }

    array_multisort($sorted, SORT_DESC, $dates);
    return $dates;
}
