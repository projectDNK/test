<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task1</title>
</head>
<body>
<form action="index.php" method="post">
    <label for="textarea">Input text</label>
    <textarea id="textarea" rows="2" cols="20" name="textarea"></textarea>
    <input type="submit">
</form>
</body>
</html>

<?php
if($_POST) {
    $data = $_POST['textarea'];

    $string = explode(' ', $data);
    $count = count($string);
    $result = '';
    for ($i = 0; $i < $count; $i++) {
        if (iconv_strlen($string[$i]) > 1) {
            $result = $result . mb_convert_case($string[$i], MB_CASE_TITLE, "UTF-8") . '_';
        }
        else {
                $result = $result . $string[$i] . '_';
            }
    }
    echo rtrim($result, '_');

}
?>
